
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags"%>

<template:template>

	<br />lista de posts<br />
	<ul class="list-group">
		<c:forEach items="${postLista}" var="post">
			<li class="list-group-item"><a href="post/view/${post.id}">msg="${post.msg}"</a></li>
		</c:forEach>
	</ul>

</template:template>
