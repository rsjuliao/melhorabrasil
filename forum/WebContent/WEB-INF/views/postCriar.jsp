
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags"%>
<template:template>

	<form:form method="POST"
		action="${pageContext.request.contextPath}/post/criar/sendform"
		modelAttribute="post">
		<table>
			<tr>

				<td><form:label path="msg">msg</form:label></td>
				<td><form:input path="msg" /></td>
				<td><form:hidden path="pai.id" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="submit" /></td>
			</tr>
		</table>
	</form:form>

</template:template>