<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags"%>

<!DOCTYPE html>
<html lang="pt">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bootstrap 101 Template</title>

<!-- Bootstrap -->


<link href="<c:url value="/resources/css/bootstrap.min.css"/>"
	rel="stylesheet">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="page-header">
				<h1>
					<a href="<c:url value="/"/>">melhoraBrasil</a> <small><span>
							<ul class="nav nav-pills">

								<c:if test="${empty usuario}">
									<li role="presentation" class="active"><a
										href="<c:url value="/logaFacebook"/>">logar</a> <span
										class="glyphicon glyphicon-plus" aria-hidden="true"></span></li>
								</c:if>

								<c:if test="${not empty usuario}">
									<li role="presentation"><a href="#">${usuario.nome}</a></li>

									<li role="presentation"><a
										href="<c:url value="/post/criar"/>">novo post</a></li>
									<li role="presentation"><a href="#">meus posts</a></li>

								</c:if>
					</span>
						</ul> </small>
				</h1>

			</div>
			<jsp:doBody />


			<div class="panel-footer">bmjuliao@gmail.com</div>

			<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
			<script src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
			<!-- Include all compiled plugins (below), or include individual files as needed -->
			<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>

		</div>
		<!-- conteiner -->
	</div>
	<!-- conteiner -->
</body>
</html>