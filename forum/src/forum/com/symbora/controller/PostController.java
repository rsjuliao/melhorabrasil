package forum.com.symbora.controller;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import forum.com.symbora.model.Avaliacao;
import forum.com.symbora.model.Post;
import forum.com.symbora.model.Usuario;
import forum.com.symbora.repository.AvaliacaoRepositoryInterface;
import forum.com.symbora.repository.PostRepositoryInterface;

@Controller
public class PostController {

	@RequestMapping("/post/responder/{idPai}")
	public ModelAndView criarResponderAoPai(@PathVariable("idPai") Long idPai, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		if (usuario == null) {
			return new ModelAndView("redirect:/logaFacebook");
		} else {
			Post postNovo = new Post();

			postNovo.setPai(postRepositoryInterface.findOne(idPai));
			ModelAndView modelAndView = new ModelAndView("postCriar", "post", postNovo);
			modelAndView.addObject("usuario", usuario);
			return modelAndView;
		}
	}

	@RequestMapping("/post/criar")
	public ModelAndView criarNovo(HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		if (usuario == null) {
			return new ModelAndView("redirect:/logaFacebook");
		} else {
			return new ModelAndView("postCriar", "post", new Post());
		}
	}

	@Inject
	PostRepositoryInterface postRepositoryInterface;
	@Inject
	AvaliacaoRepositoryInterface avaliacaoRepositoryInterface;

	@RequestMapping(value = "/post/criar/sendform", method = RequestMethod.POST)
	public String criarNovoPost(@ModelAttribute("Post") Post post, HttpSession session, ModelMap model) {

		Usuario usuario = (Usuario) session.getAttribute("usuario");
		if (usuario == null) {
			return "redirect:/logaFacebook";
		} else {
			post.setDono(usuario);
			if (post.getPai() != null) {
				if (post.getPai().getId() == null) {
					post.setPai(null);
				}
			}

			postRepositoryInterface.save(post);
			return "redirect:/post/view/" + post.getId();
		}
	}

	@RequestMapping("/post/avaliar/{idPost}")
	public ModelAndView criarAvaliar(@PathVariable("idPost") Long idPost, HttpSession session) {
		Post post = postRepositoryInterface.findOne(idPost);
		Avaliacao avaliacao = avaliacaoRepositoryInterface.findFirstBypostAvaliadoAndUsuarioAvaliador(post, (Usuario) session.getAttribute("usuario"));
		ModelAndView modelAndView;
		if (avaliacao == null) {
			avaliacao = new Avaliacao();
			avaliacao.setPostAvaliado(post);
			avaliacao.setUsuarioAvaliador((Usuario) session.getAttribute("usuario"));
			modelAndView = new ModelAndView("avaliarForm", "avaliacao", avaliacao);
		} else {
			modelAndView = new ModelAndView("redirect:/post/responder/" + idPost, "post", post);
		}

		return modelAndView;

	}

	@RequestMapping(value = "/post/avaliar/sendform", method = RequestMethod.POST)
	public String persistirAvaliacao(@ModelAttribute("avaliacao") Avaliacao avaliacao, HttpSession session) {
		Post post = postRepositoryInterface.findOne(avaliacao.getPostAvaliado().getId());
		avaliacao.setPostAvaliado(post);
		avaliacao.setUsuarioAvaliador((Usuario) session.getAttribute("usuario"));
		avaliacaoRepositoryInterface.save(avaliacao);
		return "redirect:/post/avaliar/" + post.getId();
	}

	@RequestMapping(value = "/post/view/{id}")
	public ModelAndView view(@PathVariable("id") Long id) {
		Post postFound = postRepositoryInterface.findOne(id);
		ModelAndView modelAndView = new ModelAndView("postView", "post", postFound);
		modelAndView.addObject("postsFilhos", postRepositoryInterface.findByPaiId(id));

		LinkedList<Post> posts = new LinkedList<Post>();
		for (Post p = postFound; p != null; p = p.getPai()) {
			posts.addFirst(p);
		}
		modelAndView.addObject("postsPais", posts);

		return modelAndView;
	}

	@RequestMapping(value = "/post/usuario")
	public ModelAndView postsDoUsuario(HttpSession session) {
		ModelAndView mav = new ModelAndView("postsUsuario");
		List<Post> posts = postRepositoryInterface.findByDono((Usuario) session.getAttribute("usuario"));
		mav.addObject("posts", posts);

		return mav;

	}

}
