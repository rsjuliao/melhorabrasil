package forum.com.symbora.controller.facebook;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import forum.com.symbora.model.Usuario;
import forum.com.symbora.repository.UsuarioRepositoryInterface;

@Controller
public class FacebookController {
	public FacebookController() {
		fbConnectionFactory = SocialConfig.getInstance()
				.getFacebookConnectionFactory();
		oauthOperations = fbConnectionFactory.getOAuthOperations();

	}

	private static final String CALLBACK_URL = "http://localhost:8080/forum/logaFacebookStep2";

	FacebookConnectionFactory fbConnectionFactory = null;

	OAuth2Operations oauthOperations = null;

	@RequestMapping({ "/logaFacebook" })
	public String LoginStep1(HttpSession session) {
		OAuth2Parameters params = new OAuth2Parameters();
		params.setRedirectUri(CALLBACK_URL);
		params.setScope("user_about_me");

		String authorizeUrl = oauthOperations.buildAuthorizeUrl(
				GrantType.AUTHORIZATION_CODE, params);

		return "redirect:" + authorizeUrl;

	}

	@Inject
	UsuarioRepositoryInterface UsuarioRepositoryInterface;

	@RequestMapping({ "/logaFacebookStep2" })
	public String LoginStep2(String code, HttpSession session) {

		AccessGrant accessGrant = oauthOperations.exchangeForAccess(code,
				CALLBACK_URL, null);
		Facebook facebook = new FacebookTemplate(accessGrant.getAccessToken());

		// fazer aqui o cadastro do usuario.
		FacebookProfile perfilFb = facebook.userOperations().getUserProfile();

		// verifica se o usuario já existe no banco de dados.
		Usuario usuario = UsuarioRepositoryInterface
				.findFirstByFacebookId(perfilFb.getId());

		if (usuario != null) {// usuario antigo
			session.setAttribute("usuario", usuario);
			return "redirect:/";
		} else {// primeira vez do usuario
			Usuario novoUsuario = new Usuario();
			novoUsuario.setNome(perfilFb.getName());
			novoUsuario.setFacebookId(perfilFb.getId());

			UsuarioRepositoryInterface.save(novoUsuario);
			session.setAttribute("usuario", novoUsuario);
			return "redirect:/";
		}

	}

	@RequestMapping({ "/deslogaFacebook" })
	public String deslogaFacebook(HttpSession session) {
		session.setAttribute("usuario", null);
		return "redirect:/";

	}
}