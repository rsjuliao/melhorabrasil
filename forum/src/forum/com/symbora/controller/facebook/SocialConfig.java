package forum.com.symbora.controller.facebook;

import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;


public class SocialConfig {
	private static SocialConfig instance = null;

	private SocialConfig() {
	}

	public static synchronized SocialConfig getInstance() {
		if (instance == null) {
			instance = new SocialConfig();
		}
		return instance;
	}

	private ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
	private String appID = "901995106517466";
	private String appSecretKey = "228c4ea71410aa1eb61414ace5e5e29c";

	private FacebookConnectionFactory connectionFactory = new FacebookConnectionFactory(
			appID, appSecretKey);
	
	public FacebookConnectionFactory getFacebookConnectionFactory(){
		return connectionFactory;
	}
}