package forum.com.symbora.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import forum.com.symbora.model.Usuario;
import forum.com.symbora.repository.PostRepositoryInterface;
import forum.com.symbora.repository.UsuarioRepositoryInterface;

@Controller
public class ControladorUtilitario {

	@Inject
	private UsuarioRepositoryInterface usuarioRepository;

	@Inject
	PostRepositoryInterface postRepositoryInterface;

	@RequestMapping({ "/index", "/" })
	public String home(HttpSession session, ModelMap model) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		model.addAttribute("usuario", usuario);
		model.addAttribute("postLista", postRepositoryInterface.findByPaiIsNull());
		return "index";
	}
}
