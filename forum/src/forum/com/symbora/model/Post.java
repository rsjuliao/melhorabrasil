package forum.com.symbora.model;

import java.util.LinkedList;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.web.bind.annotation.ModelAttribute;

@Entity
public class Post {
	@Id
	@GeneratedValue
	Long id;

	@OneToOne
	Post pai;

	@OneToOne
	Usuario dono;

	String msg;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Post getPai() {
		return pai;
	}

	public void setPai(Post pai) {
		this.pai = pai;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Usuario getDono() {
		return dono;
	}

	public void setDono(Usuario dono) {
		this.dono = dono;
	}

}
