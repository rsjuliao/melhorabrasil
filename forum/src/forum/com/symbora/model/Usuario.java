package forum.com.symbora.model;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.context.annotation.Primary;

@Entity
public class Usuario {
	@Id
	@GeneratedValue
	Long id;

	String nome;

	private String facebookId;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Primary
	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;

	}

	public String getFacebookId() {
		return this.facebookId;

	}

}
