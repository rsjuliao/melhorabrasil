package forum.com.symbora.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Avaliacao {

	@Id
	@GeneratedValue
	Long id;

	@OneToOne
	Post postAvaliado;

	@OneToOne
	Usuario usuarioAvaliador;

	byte util;
	byte coerente;
	byte fundamentado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Post getPostAvaliado() {
		return postAvaliado;
	}

	public void setPostAvaliado(Post postAvaliado) {
		this.postAvaliado = postAvaliado;
	}

	public Usuario getUsuarioAvaliador() {
		return usuarioAvaliador;
	}

	public void setUsuarioAvaliador(Usuario usuarioAvaliador) {
		this.usuarioAvaliador = usuarioAvaliador;
	}

	public byte getUtil() {
		return util;
	}

	public void setUtil(byte util) {
		this.util = util;
	}

	public byte getCoerente() {
		return coerente;
	}

	public void setCoerente(byte coerente) {
		this.coerente = coerente;
	}

	public byte getFundamentado() {
		return fundamentado;
	}

	public void setFundamentado(byte fundamentado) {
		this.fundamentado = fundamentado;
	}

}
