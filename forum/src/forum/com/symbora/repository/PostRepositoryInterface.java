package forum.com.symbora.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import forum.com.symbora.model.Post;
import forum.com.symbora.model.Usuario;

public interface PostRepositoryInterface extends JpaRepository<Post, Long> {
	public List<Post> findByPaiIsNull();

	public List<Post> findByPaiId(Long id);
	
	public List<Post> findByDono(Usuario dono);
}
