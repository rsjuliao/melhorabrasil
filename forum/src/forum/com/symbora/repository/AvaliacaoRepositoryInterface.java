package forum.com.symbora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import forum.com.symbora.model.Avaliacao;
import forum.com.symbora.model.Post;
import forum.com.symbora.model.Usuario;

public interface AvaliacaoRepositoryInterface extends JpaRepository<Avaliacao, Long> {
	public Avaliacao findFirstBypostAvaliadoAndUsuarioAvaliador(Post post, Usuario usuarioAvaliador);

}
