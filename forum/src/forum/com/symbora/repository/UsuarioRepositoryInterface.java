package forum.com.symbora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import forum.com.symbora.model.Usuario;

public interface UsuarioRepositoryInterface extends JpaRepository<Usuario, Long>   {
	
	public Usuario findFirstByFacebookId(String facebookID);

}
