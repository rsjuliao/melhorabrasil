package rsjuliao.upnp;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;

public class Util {
	public static LinkedList<String> getInterfaces() {

		Enumeration<NetworkInterface> nets = null;
		LinkedList<String> enderecos = new LinkedList<String>();

		try {
			nets = NetworkInterface.getNetworkInterfaces();
			for (NetworkInterface netint : Collections.list(nets)) {
				displayInterfaceInformation(netint, enderecos);
			}
		} catch (SocketException e) {
			e.printStackTrace();
			return null;
		}

		return enderecos;

	}

	private static void displayInterfaceInformation(NetworkInterface netint, LinkedList<String> enderecos)
			throws SocketException {
		Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();

		for (InetAddress inetAddress : Collections.list(inetAddresses)) {
			if (false == inetAddress.isLoopbackAddress()) {
					enderecos.add(inetAddress.getHostAddress());
			}
		}

	}
}
