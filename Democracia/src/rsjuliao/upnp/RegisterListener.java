package rsjuliao.upnp;

import org.fourthline.cling.model.meta.LocalDevice;
import org.fourthline.cling.model.meta.RemoteDevice;
import org.fourthline.cling.registry.Registry;
import org.fourthline.cling.registry.RegistryListener;

public class RegisterListener implements RegistryListener {
	public void remoteDeviceDiscoveryStarted(Registry registry, RemoteDevice device) {
		System.out.println("Discovery started: " + device.getDisplayString());
	}

	public void remoteDeviceDiscoveryFailed(Registry registry, RemoteDevice device, Exception ex) {
		System.out.println("Discovery failed: " + device.getDisplayString() + " => " + ex);
	}

	public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
		System.out.println("Remote device available: " + device.getDisplayString());
	}

	public void remoteDeviceUpdated(Registry registry, RemoteDevice device) {
		System.out.println("Remote device updated: " + device.getDisplayString());
	}

	public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
		System.out.println("Remote device removed: " + device.getDisplayString());
	}

	public void localDeviceAdded(Registry registry, LocalDevice device) {
		System.out.println("Local device added: " + device.getDisplayString());
	}

	public void localDeviceRemoved(Registry registry, LocalDevice device) {
		System.out.println("Local device removed: " + device.getDisplayString());
	}

	public void beforeShutdown(Registry registry) {
		System.out.println("Before shutdown, the registry has devices: " + registry.getDevices().size());
	}

	public void afterShutdown() {
		System.out.println("Shutdown of registry complete!");

	}

}
