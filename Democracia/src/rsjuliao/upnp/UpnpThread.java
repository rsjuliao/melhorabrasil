package rsjuliao.upnp;

import java.util.LinkedList;

import org.fourthline.cling.UpnpService;
import org.fourthline.cling.UpnpServiceImpl;

import rsjuliao.perfil.Perfil;
import rsjuliao.socket.GerenciadorServerSockets;

public class UpnpThread extends Thread {

	public void run() {
		try {

			final UpnpService upnpService = new UpnpServiceImpl(new RegisterListenerMelhoraBrasil(linkedListInterfaces,gerenciadorSockets,perfilEu));

			
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					upnpService.shutdown();
				}
			});

			// Add the bound local device to the registry
			upnpService.getRegistry().addDevice((new BinaryLightServer()).createDevice(linkedListInterfaces,gerenciadorSockets,perfilEu));

		} catch (Exception ex) {
			System.err.println("Exception occured: " + ex);
			ex.printStackTrace(System.err);
			System.exit(1);
		}
	}

	LinkedList<String> linkedListInterfaces;
	GerenciadorServerSockets gerenciadorSockets;
	private Perfil perfilEu;

	public UpnpThread(LinkedList<String> linkedListInterfaces, GerenciadorServerSockets gerenciadorSockets, Perfil perfilEu) {
		this.linkedListInterfaces = linkedListInterfaces;
		this.gerenciadorSockets = gerenciadorSockets;
		this.perfilEu=perfilEu;
		
		meAnuncie();
	
	}

	

	public void meAnuncie() {
		//essa parte é feita no metodo run
		//esse comentario deve ficar aqui eternametne para evitar boberas futuras
	}

	public void getPeers() {
		// TODO Auto-generated method stub

	}

}
