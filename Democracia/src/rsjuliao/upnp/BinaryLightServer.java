package rsjuliao.upnp;

import java.io.IOException;
import java.util.LinkedList;

import org.fourthline.cling.UpnpService;
import org.fourthline.cling.UpnpServiceImpl;
import org.fourthline.cling.binding.LocalServiceBindingException;
import org.fourthline.cling.binding.annotations.AnnotationLocalServiceBinder;
import org.fourthline.cling.model.DefaultServiceManager;
import org.fourthline.cling.model.ValidationException;
import org.fourthline.cling.model.meta.DeviceDetails;
import org.fourthline.cling.model.meta.DeviceIdentity;
import org.fourthline.cling.model.meta.Icon;
import org.fourthline.cling.model.meta.LocalDevice;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.meta.ManufacturerDetails;
import org.fourthline.cling.model.meta.ModelDetails;
import org.fourthline.cling.model.types.DeviceType;
import org.fourthline.cling.model.types.UDADeviceType;
import org.fourthline.cling.model.types.UDN;

import rsjuliao.criptografia.Md5;
import rsjuliao.perfil.Perfil;
import rsjuliao.socket.GerenciadorServerSockets;

public class BinaryLightServer implements Runnable {

	public void run() {
		try {

			final UpnpService upnpService = new UpnpServiceImpl();

			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					upnpService.shutdown();
				}
			});

			// Add the bound local device to the registry
			upnpService.getRegistry().addDevice(createDevice(linkedListInterfaces, gerenciadorSockets,perfilEu));

		} catch (Exception ex) {
			System.err.println("Exception occured: " + ex);
			ex.printStackTrace(System.err);
			System.exit(1);
		}
	}

	LinkedList<String> linkedListInterfaces;
	GerenciadorServerSockets gerenciadorSockets;
	private Perfil perfilEu;

	public LocalDevice createDevice(LinkedList<String> linkedListInterfaces, GerenciadorServerSockets gerenciadorSockets,
			Perfil perfilEu) throws ValidationException, LocalServiceBindingException, IOException {

		this.linkedListInterfaces = linkedListInterfaces;
		this.gerenciadorSockets = gerenciadorSockets;
		this.perfilEu = perfilEu;

		String nome = perfilEu.nome;
		String descricao = perfilEu.enderecoVirtual;

		byte[] md5EnderecoVirtual=Md5.vetorBytesToMD5(descricao.getBytes());
		DeviceIdentity identity = new DeviceIdentity(UDN.uniqueSystemIdentifier(md5EnderecoVirtual.toString()));

		DeviceType type = new UDADeviceType("BinaryLight", 1);

		DeviceDetails details = new DeviceDetails(nome, new ManufacturerDetails("Melhora Brasil 2.0"),
				new ModelDetails(descricao));

		Icon icon = new Icon("image/png", 48, 48, 8, BinaryLightServer.class.getResource("brasil.jpg"));

		LocalService<SwitchPower> switchPowerService = new AnnotationLocalServiceBinder().read(SwitchPower.class);

		switchPowerService.setManager(new DefaultServiceManager(switchPowerService, SwitchPower.class));
		
		
		return new LocalDevice(identity, type, details, icon, switchPowerService);

		/*
		 * Several services can be bound to the same device: return new
		 * LocalDevice( identity, type, details, icon, new LocalService[]
		 * {switchPowerService, myOtherService} );
		 */

	}

}