package rsjuliao.socket;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;

import javax.sql.rowset.spi.TransactionalWriter;

import rsjuliao.perfil.Perfil;
import rsjuliao.util.Serializacao;

public class GerenciadorClienteSocket {

	private Perfil perfilEu;

	public GerenciadorClienteSocket(Perfil perfilEu) {
		this.perfilEu = perfilEu;
	}

	private void enviaBytes(byte[] informacao) {
		try {
			if(informacao==null){
				System.out.println(" !------! PARABENS SEU FILHO DA PUTA, VOCE TA TENTAND OENVIAR NULL NOVAMNETE");
			}

			int tamanho = informacao.length;
			perfilEu.socket.getOutputStream().write(tamanho);
			perfilEu.socket.getOutputStream().write(informacao);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("É filhote, parece que houve um erro no envio de informacao");
		}
	}

	private byte[] recebeBytes() {
		try {

			int tam = perfilEu.socket.getInputStream().read();
			// esse read aqui nunca responde
			byte[] informacaoTamExato = new byte[tam];
			perfilEu.socket.getInputStream().read(informacaoTamExato, 0, tam);
			return informacaoTamExato;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	//
	//
	public void enviaObjeto(Serializable objeto) {

		System.out.println(" #>># " + perfilEu.nome + "\t\t enviando objeto:" + objeto.getClass().getSimpleName());
		byte[] requisitaPerfilSerializado = Serializacao.serializa(objeto);
		enviaBytes(requisitaPerfilSerializado);

		// no futuro, em algum momento, o perfil será enviado
	}

	//
	//
	public Serializable recebeObjeto() {
		Serializable objeto = Serializacao.objetifica(recebeBytes());
		System.out.println(" #<<# " + perfilEu.nome + "\t\t recebendo objeto:" + objeto.getClass().getSimpleName());

		return objeto;
	}

}
