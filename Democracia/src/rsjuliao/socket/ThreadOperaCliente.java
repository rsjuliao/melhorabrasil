package rsjuliao.socket;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;

import rsjuliao.perfil.Perfil;
import rsjuliao.socket.comunicacao.Requisicao;

public class ThreadOperaCliente extends Thread {
	private Perfil perfilEu;

	public ThreadOperaCliente(Perfil perfilEu) {

		this.perfilEu = perfilEu;
	}

	@Override
	public void run() {

		if (perfilEu.socket == null) {
			instanciaSocket();
		}

		//
		GerenciadorClienteSocket gerenciadorClienteSocket = new GerenciadorClienteSocket(perfilEu);
		// requisita perfil
		gerenciadorClienteSocket.enviaObjeto(new Requisicao("Perfil"));
		//

		// goto
		recomecaLoop: // label para resetar o loops
		// goto

		// recebe objetos
		while (true) {
			Serializable objeto = gerenciadorClienteSocket.recebeObjeto();

			if (objeto instanceof Requisicao) {
				Requisicao requisicao = (Requisicao) objeto;
				if (requisicao.requisicao.compareTo("Perfil") == 0) {
					System.out.println("um perfil foi requisitado");
					gerenciadorClienteSocket.enviaObjeto(perfilEu);
				}

			}
			if (objeto instanceof Perfil) {
				Perfil p = (Perfil) objeto;
				System.out.println("***parabens um perfil foi recebido com sucesso***");
				System.out.println(p.nome);
				System.out.println(p.enderecoVirtual);

			}

		}
	}

	private void instanciaSocket() {
		// separa os ips e tenta conexao com cada um deles
		System.out.println("vamos iniciar uma nova conexao com um servidor ;) A brincadeira começa agora!");
		String[] ips = perfilEu.enderecoVirtual.split(" ");
		if (ips.length > 1) {
			int porta = Integer.parseInt(ips[ips.length - 1]);
			for (int aux = 0; aux < ips.length - 1; aux++) {
				String ipHost = ips[aux];
				try {
					perfilEu.socket = new Socket(ipHost, porta);
					System.out.println("*parabens, voce realizou uma conexao com sucessoF=D*");
					break;// interrompe o laco for e sai com o socket criado
				} catch (IOException e) {
					perfilEu.socket = null;
					// e.printStackTrace();
					System.out.println("alguma coisa deu errado na tentativa de se conectar ao servidor!");
				}
			}
		}

	}
}
