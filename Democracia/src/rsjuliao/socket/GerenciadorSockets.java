package rsjuliao.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

public class GerenciadorSockets {

	public ServerSocket serverSocket;
	public ConcurrentLinkedQueue<Socket> clientesSockets = new ConcurrentLinkedQueue<Socket>();
	ListnerServerSocket listnerServerSocketConcurrent = null;

	public GerenciadorSockets() {
		while (true) {
			int porta = 1024 + new Random().nextInt(65536 - 1024);
			try {
				serverSocket = new ServerSocket(porta);
				break;// sai do while
			} catch (IOException e) {
				e.printStackTrace();
				System.out
						.println("aparentemente essa porta já esta em uso. vou tentar outra aleatória imediatamente!");
			}
		}

		listnerServerSocketConcurrent = new ListnerServerSocket(serverSocket, clientesSockets);
		listnerServerSocketConcurrent.start();
	}

}

class ListnerServerSocket extends Thread {
	ServerSocket serverSocket;
	ConcurrentLinkedQueue<Socket> clientesSocketsConcurrent;

	public ListnerServerSocket(ServerSocket serverSocket, ConcurrentLinkedQueue<Socket> clientesSocketsConcurrent) {
		this.serverSocket = serverSocket;
		this.clientesSocketsConcurrent = clientesSocketsConcurrent;
	}

	@Override
	public void run() {
		try {
			System.out.println("o socket servidor esta pronto e esperando conexao");
			Socket cliente = serverSocket.accept();
			System.out.println(
					"cliente adicionado: " + cliente.getInetAddress().getHostAddress() + ":" + cliente.getPort());
			clientesSocketsConcurrent.add(cliente);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
