package rsjuliao.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import rsjuliao.perfil.Perfil;

public class GerenciadorServerSockets {

	// server
	public ServerSocket serverSocket;
	//

	// clintes
	public ConcurrentLinkedQueue<Perfil> concurrentListaPerfil = new ConcurrentLinkedQueue<Perfil>();
	//

	//
	ListnerServerSocket listnerServerSocketConcurrent = null;

	public GerenciadorServerSockets() {
		while (true) {
			int porta = 1024 + new Random().nextInt(65536 - 1024);
			try {
				serverSocket = new ServerSocket(porta);
				break;// sai do while
			} catch (IOException e) {
				e.printStackTrace();
				System.out
						.println("aparentemente essa porta já esta em uso. vou tentar outra aleatória imediatamente!");
			}
		}

		listnerServerSocketConcurrent = new ListnerServerSocket(serverSocket, concurrentListaPerfil);
		listnerServerSocketConcurrent.start();

	}

	public void addPossivelCliente(Perfil perfil) {
		concurrentListaPerfil.add(perfil);
		perfil.abreConexao();
	}

}

class ListnerServerSocket extends Thread {
	ServerSocket serverSocket;
	ConcurrentLinkedQueue<Socket> clientesSocketsConcurrent;
	private ConcurrentLinkedQueue<Perfil> concurrentListaPerfil;

	public ListnerServerSocket(ServerSocket serverSocket, ConcurrentLinkedQueue<Perfil> concurrentListaPerfil) {
		this.serverSocket = serverSocket;
		this.concurrentListaPerfil = concurrentListaPerfil;

		this.clientesSocketsConcurrent = clientesSocketsConcurrent;
	}

	@Override
	public void run() {
		try {

			System.out.println("o socket servidor esta pronto e esperando conexao");
			
			//
			//
			Socket clienteSocket = serverSocket.accept();
			//
			//
			System.out.println("SO ENTRA NESSA PARTE DEPOIS QUE UM CLIENTE TENTAR CONEXAO");

			Perfil perfilCliente = new Perfil(clienteSocket);
			perfilCliente.abreConexao();
			concurrentListaPerfil.add(perfilCliente);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
