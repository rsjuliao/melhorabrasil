package rsjuliao.perfil;

import java.io.Serializable;
import java.net.Socket;

import rsjuliao.socket.ThreadOperaCliente;

public class Perfil implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public String nome = "anonimo";
	public String enderecoVirtual = "";
	public String identificadorUnico = "";
	public transient Socket socket = null;

	public Perfil(String nome, String enderecoVirtual) {
		this.nome = nome;
		this.enderecoVirtual = enderecoVirtual;
	}

	public Perfil(Socket socket) {
		this.socket = socket;

	}

	public void abreConexao() {
		ThreadOperaCliente threadOperaCliente = new ThreadOperaCliente(this);
		threadOperaCliente.start();
	}

}

//
//
