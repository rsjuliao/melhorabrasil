package rsjuliao.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;

public abstract class Serializacao {

	public static byte[] serializa(Serializable objeto) {

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(objeto);
			byte[] serializado = bos.toByteArray();
			
			return serializado;
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("amigao, o que voce anda fazendo errado na hora de serializar um objeto?");
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException ex) {
				// ignore close exception
			}
			try {
				bos.close();
			} catch (IOException ex) {
				// ignore close exception
			}
		}
		return null;

	}

	public static Serializable objetifica(byte[] objSerializado) {
		
		ByteArrayInputStream bis = new ByteArrayInputStream(objSerializado);
		ObjectInput in = null;
		try {
			in = new ObjectInputStream(bis);
			Serializable objeto = (Serializable) in.readObject();
			return objeto;

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				bis.close();
			} catch (IOException ex) {
				// ignore close exception
			}
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				// ignore close exception
			}
		}

		return null;
	}

}
